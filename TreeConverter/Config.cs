﻿using System.Text.RegularExpressions;
using System.IO;

namespace TreeConverter
{
    class Config
    {
        public string Kuid { get; set; }
        public string Username { get; set; }
        public string Filename { get; set; }


        public void LoadConfig(string file)
        {
            var content = File.ReadAllText(file);

            var kuidRegex = new Regex(@"kuid\s*<kuid:([0-9]+:[0-9]+)>");
            var usernameRegex = new Regex(@"username\s*""(.+)""");
            var filenameRegex = new Regex(@"mesh\s*""(\w+).im""");

            var kuidMatch = kuidRegex.Match(content);
            var usernameMatch = usernameRegex.Match(content);
            var filenameMatch = filenameRegex.Match(content);

            if (kuidMatch.Success)
            {
                Kuid = kuidMatch.Groups[1].Value;
            }
            if (usernameMatch.Success)
            {
                Username = usernameMatch.Groups[1].Value;
            }
            if (filenameMatch.Success)
            {
                Filename = filenameMatch.Groups[1].Value;
            }
        }

        public void SaveConfig(string file)
        {
            var content = $@"          
                kind                                    ""scenery""
                username                                ""{Options.UsernamePrefix} {Username}""
                rotate-yz-range                         -90,90
                rollstep                                0.1
                trainz-build                            2
                category-class                          ""FT""
                category-region                         ""CS;CZ;DE;NL;PL;RU;SK;UK""
                category-era                            ""2000s""
                asset-filename                          ""{Filename}""
                kuid                                    <kuid:{Kuid}>
            ";
            File.WriteAllText(file, content);
        }
    }
}
