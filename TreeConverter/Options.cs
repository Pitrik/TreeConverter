﻿namespace TreeConverter
{
    public static class Options
    {
        public static string WorkingDirectory { get; set; }
        public static string UsernamePrefix { get; set; }
        public static int TextureThreshold { get; set; }
        public static int OutputSize { get; set; }
    }
}
