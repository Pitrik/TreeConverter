﻿using ImageMagick;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace TreeConverter
{
    class Bitmap
    {
        private MagickImage _image;
        
        public System.Drawing.Bitmap BitmapImage { get; private set; }

        public Bitmap(string file)
        {
            _image = new MagickImage();
            _image.Read(file);
            BitmapImage = _image.ToBitmap();
        }

        public void Convert()
        {
            int n = Options.TextureThreshold;
            for (int x = 0; x < BitmapImage.Width; x++)
            {
                for (int y = 0; y < BitmapImage.Height; y++)
                {
                    Color C = BitmapImage.GetPixel(x, y);

                    byte R = C.R;
                    byte G = C.G;
                    byte B = C.B;
                    byte A = C.A;

                    int r = Math.Min(Math.Max(0, R + n), 255);
                    int g = Math.Min(Math.Max(0, G + n), 255);
                    int b = Math.Min(Math.Max(0, B + n), 255);

                    Color c = Color.FromArgb(A, r, g, b);
                    BitmapImage.SetPixel(x, y, c);
                }
            }
        }

        public void ResizeImage()
        {
            int width = Options.OutputSize;
            int height = Options.OutputSize;
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new System.Drawing.Bitmap(width, height);

            destImage.SetResolution(BitmapImage.HorizontalResolution, BitmapImage.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(BitmapImage, destRect, 0, 0, BitmapImage.Width, BitmapImage.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            BitmapImage = destImage;
        }

        public void Save(string file)
        {
            _image.Read(BitmapImage);
            _image.Write(file);
        }
    }
}
