﻿using System.IO;

namespace TreeConverter
{
    public class Converter
    {
        private string _workingDirectory;

        public Converter(string workingDirectory)
        {
            _workingDirectory = workingDirectory;
        }

        public string[] GetConfigFiles
        {
            get
            {
                return Directory.GetFiles(_workingDirectory, "config.txt", SearchOption.AllDirectories);
            }
        }

        public string[] GetTextureFiles
        {
            get
            {
                return Directory.GetFiles(_workingDirectory, "*.texture.txt", SearchOption.AllDirectories);

            }
        }

        public string[] GetBitmapTextureFiles
        {
            get
            {
                return Directory.GetFiles(_workingDirectory, "*.tga", SearchOption.AllDirectories);
            }
        }
    }
}
