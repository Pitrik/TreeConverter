﻿using System;

namespace TreeConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Set working directory: ");
            string directory = Console.ReadLine();
            //string directory = @"D:\Auran\TS2009\UserData\editing";
            Options.WorkingDirectory = directory;
            Console.WriteLine($"Set {directory} as working directory.");
            Console.WriteLine("Set texture threshold: ");
            Options.TextureThreshold = int.Parse(Console.ReadLine());
            Console.WriteLine("Set output texture size (recomended 2048 - 4096px (medium quality - hight quality): ");
            Options.OutputSize = int.Parse(Console.ReadLine());
            Console.WriteLine("Set output username prefix (empty string is possible): ");
            Options.UsernamePrefix = Console.ReadLine();

            Console.WriteLine("Processing config files, please wait...");
            var converter = new Converter(directory);
            foreach (string configFile in converter.GetConfigFiles)
            {
                var config = new Config();
                config.LoadConfig(configFile);
                config.SaveConfig(configFile);
                Console.Write("#");
            }
            Console.WriteLine("Done");

            Console.WriteLine("Processing texture files, please wait...");
            foreach (string textureFile in converter.GetTextureFiles)
            {
                var texture = new Texture();
                texture.LoadTexture(textureFile);
                texture.SaveTexture(textureFile);
                Console.Write("#");
            }
            Console.WriteLine("Done");

            Console.WriteLine("Processing bitmap files, please wait...");
            foreach (string bitmapFile in converter.GetBitmapTextureFiles)
            {
                var bitmap = new Bitmap(bitmapFile);
                bitmap.Convert();
                bitmap.ResizeImage();
                bitmap.Save(bitmapFile);
                Console.Write("#");
            }
            Console.WriteLine("Done");
            Console.ReadKey();
        }
    }
}
