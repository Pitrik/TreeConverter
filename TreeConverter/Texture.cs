﻿using System.IO;
using System.Text.RegularExpressions;

namespace TreeConverter
{
    class Texture
    {
        public string Primary { get; set; }
        public string Alpha { get; set; }

        public void LoadTexture(string file)
        {
            var content = File.ReadAllText(file);

            var primaryRegex = new Regex(@"Primary=(.+)");
            var alphaRegex = new Regex(@"Alpha=(.+)");

            var primaryMatch = primaryRegex.Match(content);
            var alphaMatch = alphaRegex.Match(content);

            if (primaryMatch.Success)
            {
                Primary = primaryMatch.Groups[1].Value; 
            }
            if (alphaMatch.Success)
            {
                Alpha = alphaMatch.Groups[1].Value;
            }
        }

        public void SaveTexture(string file)
        {
            var content = $@"
                Primary={Primary}
                Alpha={Alpha}
                Tile=st
            ";
            File.WriteAllText(file, content);
        }
    }
}
